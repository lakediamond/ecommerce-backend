FROM golang:1.11.2-alpine3.8

RUN apk add --no-cache --virtual .build-deps \
    alpine-sdk \
    cmake \
    sudo \
    libssh2 libssh2-dev\
    git \
    xz

WORKDIR $GOPATH/src/gitlab.com/lakediamond

# set environment path
ENV GO111MODULE=on
ENV PATH /go/bin:$PATH

COPY go.mod .
COPY go.sum .

RUN go mod download -json

ADD . .

RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o ecommerce-backend cmd/lake_diamond/main.go

# get app & install
#COPY . .
#RUN go build -i -v -o $GOPATH/bin/ecommerce-backend $GOPATH/src/gitlab.com/lakediamond/cmd/lake_diamond/main.go

CMD ["./ecommerce-backend"]
