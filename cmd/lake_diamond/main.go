package main

import (
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/httpserver"
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/listener"
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/moltin"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

func main() {
	//Getting application
	app := application.NewApplication()
	//Initialising moltin dependencies
	moltin.InitMotlin(app)
	//Starting event listener
	go listener.RunListener(app)
	//Starting http server
	httpserver.Serve(app)
}
