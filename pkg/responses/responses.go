// Package responses for providing various helpers for writing HTTP responses
// Reference API responses https://stripe.com/docs/api
package responses

import (
	"net/http"
)

func JSON(w http.ResponseWriter, statusCode int, data []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(data)
}
