package httputil

import (
	"io"
	"io/ioutil"
)

// ReadBody return request body or empty string
func ReadBody(body io.ReadCloser) []byte {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		return []byte("")
	}
	return b
}

// ReadRequestBodyAsString return request body as a string or empty string
func ReadRequestBodyAsString(body io.ReadCloser) string {
	return string(ReadBody(body))
}
