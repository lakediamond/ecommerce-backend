package application

import (
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/rs/zerolog/log"
	"time"
)

type EthConfig struct {
	EthClient *ethclient.Client
	EthFilter *ethereum.FilterQuery
}

func (app *Application) SetRpc() {
	log.Info().Msg("Setting WS connection")
	ethConn, err := ethclient.Dial(app.Config.RpcPort)
	if err != nil {
		log.Error().Err(err).Msg("Infura connection broken. Reconnecting in 5 second")
		time.Sleep(5000 * time.Millisecond)
		app.SetRpc()
		return
	}
	app.EthConfig.EthClient = ethConn
}
