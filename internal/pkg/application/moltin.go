package application

import (
	"strings"
	"time"
)

type MoltinConfig struct {
	BaseUrl      string
	ClientId     string
	ClientSecret string
}

type Moltin struct {
	MoltinToken                  *MoltinToken
	EntriesData                  *EntitiesData
	Events                       *Events
	TokenContractConfigEntrySlug string
}

type MoltinToken struct {
	AccessToken string `json:"access_token,omitempty"`
	Expires     int64  `json:"expires,omitempty"`
}

type EntitiesData struct {
	Entities []Entities `json:"data,omitempty"`
}

type Entities struct {
	TokenContractAddress string `json:"token_contract_address"`
	TokenSymbol          string `json:"token_symbol,omitempty"`
	TokenTotalSupply     string `json:"token_total_supply"`
	LastBlockScanned     string `json:"last_block_scanned"`
	EntriesId            string `json:"entries_id"`
}

type Events struct {
	CurrencyEventHandlerName      string
	CurrencyEventHandlerObservers []string
	ProductEventHandlerName       string
	ProductEventHandlerObservers  []string
	OrderEventHandlerName         string
	OrderEventHandlerObservers    []string
	EventHandlersURL              string
}

type EntitiesGenerated struct {
	Data []struct {
		ID                   string `json:"id"`
		Type                 string `json:"type"`
		TokenContractAddress string `json:"token_contract_address"`
		TokenSymbol          string `json:"token_symbol"`
		TokenInitialSupply   string `json:"token_initial_supply"`
		TokenTotalSupply     string `json:"token_total_supply"`
		LastBlockScanned     string `json:"last_block_scanned"`
		Meta                 struct {
			Timestamps struct {
				CreatedAt time.Time `json:"created_at"`
				UpdatedAt time.Time `json:"updated_at"`
			} `json:"timestamps"`
		} `json:"meta"`
		Links struct {
			Self string `json:"self"`
		} `json:"links"`
	} `json:"data"`
}

func (app *Application) setMoltin() {
	myEnv := getEnv()

	app.Moltin.TokenContractConfigEntrySlug = myEnv["Moltin_tokenContractConfigEntrySlug"]
	app.Moltin.Events.EventHandlersURL = myEnv["EventHandlers_URL"]

	app.Moltin.Events.CurrencyEventHandlerName = myEnv["CurrencyEventHandler_name"]
	app.Moltin.Events.CurrencyEventHandlerObservers = strings.Split(myEnv["CurrencyEventHandler_observers"], ",")

	var entities EntitiesData

	app.Moltin.EntriesData = &entities

}
