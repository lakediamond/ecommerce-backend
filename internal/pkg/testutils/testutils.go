package testutils

import (
	"net/http/httptest"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/httpserver"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

// NewHTTPMock return mocked application, router and server for tests
func NewHTTPMock() (*application.Application, *httprouter.Router, *httptest.Server) {
	app := application.NewApplication()
	router := httpserver.NewHTTPRouter(app)
	mockServer := httptest.NewServer(router)
	return app, router, mockServer
}
