package context

import (
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

func GetApplicationContext(r *http.Request) *application.Application {
	return r.Context().Value(application.ApplicationKey).(*application.Application)
}

func GetRequestParams(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}
