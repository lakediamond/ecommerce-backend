package httpserver_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/httputil"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/testutils"
)

func TestSpec(t *testing.T) {

	Convey("User should be created", t, func() {
		app, _, mockServer := testutils.NewHTTPMock()

		Convey("POST HTTP to /api/moltin_handler", func() {

			currencyEventPayload := map[string]interface{}{
				"id":           "16948153-60d2-4ac3-8e2f-59155b03469e",
				"triggered_by": "currency.updated",
				"attempt":      1,
				"integration": map[string]string{
					"id":               "af94a91c-730d-4b59-81f1-a34c41c51729",
					"integration_type": "webhook",
					"name":             "TokenAPI_CurrencyEventHandler",
					"description":      "Tracks currency.created, currency.updated events",
				},
				"resources": "{\"data\":{\"id\":\"c357a4a5-7a20-4bb7-9aa8-4c70c686c567\",\"type\":\"currency\",\"code\":\"TKN\",\"exchange_rate\":0.55,\"format\":\"0xb68179b47f81f951c2f5959b6aa6148fc4578845\",\"decimal_point\":\".\",\"thousand_separator\":\" \",\"decimal_places\":0,\"default\":true,\"enabled\":true,\"links\":{\"self\":\"https://api.moltin.com/currencies/c357a4a5-7a20-4bb7-9aa8-4c70c686c567\"},\"meta\":{\"timestamps\":{\"created_at\":\"2018-10-12T14:41:26.628Z\",\"updated_at\":\"2018-10-17T07:44:17.634Z\"}}}}",
			}
			tmp, _ := json.Marshal(currencyEventPayload)
			requestData := bytes.NewReader(tmp)
			resp, _ := http.Post(mockServer.URL+app.FindRoute("moltin_events"), "application/json", requestData)
			Convey("HTTP Response Status should be 201", func() {
				So(resp.Status, ShouldEqual, "201 Created")
				So(httputil.ReadRequestBodyAsString(resp.Body), ShouldEqual, "")
			})
		})
	})
}
