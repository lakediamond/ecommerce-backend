package httpserver

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/moltin"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

// NewHTTPRouter will return application router.
// Require instance of application to properly mount routes to application and pass application context to HTTP Handlers
func NewHTTPRouter(app *application.Application) *httprouter.Router {
	router := httprouter.New()

	routes := application.Routes{
		application.Route{Alias: "moltin_events", Method: "POST", Path: "/api/moltin_handler", Handler: app.WithApplicationContext(moltin.GetMoltinEvents)},
	}

	for _, route := range routes {
		router.Handler(route.Method, route.Path, route.Handler)
	}

	app.MountRoutes(routes)

	return router
}
