package listener

import (
	contextLib "context"
	"encoding/hex"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/app/lake_diamond/moltin"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
	"golang.org/x/net/context"
)

var eventChannel = make(chan types.Log)

func handleInfuraErr(app *application.Application) {
	app.SetRpc()

	RunListener(app)
}

func RunListener(app *application.Application) {
	//Await for contract address
	for {
		log.Info().Msg("Waiting for a contract")
		if len(app.EthConfig.EthFilter.Addresses) != 0 {
			log.Info().Msgf("Setting new contract %s", app.EthConfig.EthFilter.Addresses[0].String())
			break
		}
		time.Sleep(5000 * time.Millisecond)
	}

	go listenEvents(app)
	listenPastEvents(app)
}

func listenPastEvents(app *application.Application) {
	logs, filterErr := app.EthConfig.EthClient.FilterLogs(contextLib.Background(), *app.EthConfig.EthFilter)

	if filterErr != nil {
		log.Error().Err(filterErr).Msgf("failed to get filter logs for contract ")
		return
	}

	var lastBlock uint64

	for _, result := range logs {
		checkTransaction(app, result)
		lastBlock = result.BlockNumber
	}
	if lastBlock != 0 {
		moltin.UpdateEntryLastBlock(int(lastBlock+1), app)
	}

}

func listenEvents(app *application.Application) {

	s, err := app.EthConfig.EthClient.SubscribeFilterLogs(contextLib.Background(), *app.EthConfig.EthFilter, eventChannel)
	if err != nil {
		app.SetRpc()
		listenEvents(app)
	}
	errChan := s.Err()
	log.Info().Msg("Event listener started")
	for {
		select {
		case err := <-errChan:
			log.Error().Err(err).Msgf("Logs subscription error. Resetting connection")
			handleInfuraErr(app)
			break
		case l := <-eventChannel:
			checkTransaction(app, l)
			moltin.UpdateEntryLastBlock(int(l.BlockNumber+1), app)
		}
	}
}

func checkTransaction(app *application.Application, txLog types.Log) {

	value, _ := strconv.ParseInt(hex.EncodeToString(txLog.Data), 16, 64)
	if len(txLog.Topics) != 3 {
		log.Debug().Msg("wrong log type")
		return
	}

	tx, _, err := app.EthConfig.EthClient.TransactionByHash(context.Background(), txLog.TxHash)
	if err != nil {
		log.Error().Str("tx", txLog.TxHash.String()).Err(err).Msgf("transaction error")
	}
	str := hex.EncodeToString(tx.Data())

	var orderCode string

	//check method correctness, must be 84 bytes, receiver address and method sha3 should be equal with config
	if len(str) == 168 &&
		strings.ToLower(txLog.Topics[2].Hex()) == strings.ToLower("0x000000000000000000000000"+app.Config.OrderPaymentAddress[2:]) &&
		txLog.Topics[0].Hex() == "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef" {

		log.Info().Msgf("new payment by %d tokens", value)

		//throw first 68 bytes (4 method signature + 32 address + 32 uint)
		orderCode = string(str[136:])

		//form order id from another bytes
		orderCode = orderCode[:8] + "-" + orderCode[8:]
		orderCode = orderCode[:13] + "-" + orderCode[13:]
		orderCode = orderCode[:18] + "-" + orderCode[18:]
		orderCode = orderCode[:23] + "-" + orderCode[23:]

		//trying to get order from moltin platform and then process it
		moltin.GetMoltinOrder(orderCode, int(value), app)
	}
}
