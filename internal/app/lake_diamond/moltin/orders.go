package moltin

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"net/http"

	"github.com/rs/zerolog/log"

	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

type Order struct {
	Data struct {
		OrderRepaymentAccount string `json:"OrderRepaymentAccount"`
		OrderRepaymentBalance string `json:"OrderRepaymentBalance"`
		BillingAddress        struct {
			City        string `json:"city"`
			CompanyName string `json:"company_name"`
			Country     string `json:"country"`
			County      string `json:"county"`
			FirstName   string `json:"first_name"`
			LastName    string `json:"last_name"`
			Line1       string `json:"line_1"`
			Line2       string `json:"line_2"`
			Postcode    string `json:"postcode"`
		} `json:"billing_address"`
		Customer struct {
			Email string `json:"email"`
			Name  string `json:"name"`
		} `json:"customer"`
		ID    string   `json:"id"`
		Links struct{} `json:"links"`
		Meta  struct {
			DisplayPrice struct {
				WithTax struct {
					Amount    int    `json:"amount"`
					Currency  string `json:"currency"`
					Formatted string `json:"formatted"`
				} `json:"with_tax"`
				WithoutTax struct {
					Amount    int    `json:"amount"`
					Currency  string `json:"currency"`
					Formatted string `json:"formatted"`
				} `json:"without_tax"`
			} `json:"display_price"`
			Timestamps struct {
				CreatedAt string `json:"created_at"`
				UpdatedAt string `json:"updated_at"`
			} `json:"timestamps"`
		} `json:"meta"`
		Payment       string `json:"payment"`
		Relationships struct {
			Customer struct {
				Data struct {
					ID   string `json:"id"`
					Type string `json:"type"`
				} `json:"data"`
			} `json:"customer"`
			Items struct {
				Data []struct {
					ID   string `json:"id"`
					Type string `json:"type"`
				} `json:"data"`
			} `json:"items"`
		} `json:"relationships"`
		Shipping        string `json:"shipping"`
		ShippingAddress struct {
			City         string `json:"city"`
			CompanyName  string `json:"company_name"`
			Country      string `json:"country"`
			County       string `json:"county"`
			FirstName    string `json:"first_name"`
			Instructions string `json:"instructions"`
			LastName     string `json:"last_name"`
			Line1        string `json:"line_1"`
			Line2        string `json:"line_2"`
			PhoneNumber  string `json:"phone_number"`
			Postcode     string `json:"postcode"`
		} `json:"shipping_address"`
		Status string `json:"status"`
		Type   string `json:"type"`
	} `json:"data"`
}

func GetMoltinOrder(orderId string, amount int, app *application.Application) {
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/orders/" + orderId
	log.Debug().Msgf("Sending GET request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	req, err := http.NewRequest("GET", moltinUrl, nil)
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error")
		return
	}

	var order Order

	err = json.Unmarshal(data, &order)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
	}

	if order.Data.Meta.DisplayPrice.WithTax.Currency == app.Moltin.EntriesData.Entities[0].TokenSymbol &&
		order.Data.Status != "complete" {
		if order.Data.Meta.DisplayPrice.WithTax.Amount == 0 {
			log.Info().Msgf("Order %s not found", order.Data.ID)
		} else if amount >= order.Data.Meta.DisplayPrice.WithTax.Amount {
			updateMoltinOrder(order, app)
		} else {
			log.Info().Msgf("Order costs %s, but payed %s", order.Data.Meta.DisplayPrice.WithTax.Amount, amount)
		}
	}

	n := new(big.Int)
	n.SetString(app.Moltin.EntriesData.Entities[0].LastBlockScanned, 10)

	app.EthConfig.EthFilter.FromBlock = n
}

type UpdateMoltin struct {
	Data struct {
		Gateway string `json:"gateway"`
		Method  string `json:"method"`
	} `json:"data"`
}

func updateMoltinOrder(order Order, app *application.Application) {
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/orders/" + order.Data.ID + "/payments"
	log.Debug().Msgf("Sending POST request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	var data UpdateMoltin

	data.Data.Gateway = "manual"
	data.Data.Method = "authorize"

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Error().Msgf("Unmarshal data error", err)
	}

	req, err := http.NewRequest("POST", moltinUrl, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error().Msgf("http.NewRequest() error: %v\n", err)
		return
	}

	req.Header.Add("Authorization", bearer)
	req.Header.Add("Content-Type", "application/json")

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Msgf("http.Do() error: %v\n", err)
		return
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Moltin order response status: %s", resp.Status)

	if resp.Status == "200 OK" || resp.Status == "409 Conflict" {
		res, err := getMoltinStoredTransaction(order.Data.ID, app)
		if err != nil {
			log.Error().Err(err)
			return
		}
		updateMoltinTransactionStatus(order.Data.ID, res.Data[0].ID, app)
	}
}

type MoltinTransaction struct {
	Data []struct {
		ID              string `json:"id"`
		Type            string `json:"type"`
		Reference       string `json:"reference"`
		Gateway         string `json:"gateway"`
		Amount          int    `json:"amount"`
		Currency        string `json:"currency"`
		TransactionType string `json:"transaction-type"`
		Status          string `json:"status"`
		Relationships   struct {
			Order struct {
				Data struct {
					Type string `json:"type"`
					ID   string `json:"id"`
				} `json:"data"`
			} `json:"order"`
		} `json:"relationships"`
		OrderRepaymentAccountBalance string `json:"order_repayment_account_balance"`
	} `json:"data"`
}

func updateMoltinTransactionStatus(orderId, moltinTxId string, app *application.Application) {
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/orders/" + orderId + "/transactions/" + moltinTxId + "/capture"
	log.Debug().Msgf("Sending POST request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	req, err := http.NewRequest("POST", moltinUrl, nil)
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Authorization", bearer)
	req.Header.Add("Content-Type", "application/json")

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Update moltin transaction status = %s", resp.Status)
}

func getMoltinStoredTransaction(orderId string, app *application.Application) (MoltinTransaction, error) {
	var moltinTransaction MoltinTransaction

	log.Info().Msg("Getting stored transaction")

	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/orders/" + orderId + "/transactions"
	log.Debug().Msgf("Sending GET request to:", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	req, err := http.NewRequest("GET", moltinUrl, nil)
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return moltinTransaction, err
	}

	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return moltinTransaction, err
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Get stored transaction status = %s", resp.Status)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error: %v\n")
		return moltinTransaction, err
	}

	json.Unmarshal(data, &moltinTransaction)

	return moltinTransaction, nil
}
