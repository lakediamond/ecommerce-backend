package moltin

import (
	"bytes"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
	"io/ioutil"
	"net/http"
)

//type /v2/flows/TokenContractConfiguration/entries/b3dfaa50-37d7-47a6-aeaa-892980278a69

type EntryLastBlock struct {
	Data struct {
		ID               string `json:"id"`
		Type             string `json:"type"`
		LastBlockScanned int    `json:"last_block_scanned"`
	} `json:"data"`
}

///v2/flows/TokenContractConfiguration/entries/b3dfaa50-37d7-47a6-aeaa-892980278a69

func UpdateEntryLastBlock(lastBlock int, app *application.Application) {
	log.Debug().Msgf("Updating last block number to %s", lastBlock)

	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/flows/" +
		app.Moltin.TokenContractConfigEntrySlug + "/entries/" + app.Moltin.EntriesData.Entities[0].EntriesId

	log.Debug().Msgf("Sending PUT request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	var entryLastBlock EntryLastBlock

	entryLastBlock.Data.ID = app.Moltin.EntriesData.Entities[0].EntriesId
	entryLastBlock.Data.LastBlockScanned = lastBlock
	entryLastBlock.Data.Type = "entry"

	jsonStr, err := json.Marshal(entryLastBlock)

	req, err := http.NewRequest("PUT", moltinUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error: %v\n")
	}
	defer resp.Body.Close()

	var entryLastBlockResponse EntryLastBlock

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error: %v\n")
		return
	}

	json.Unmarshal(data, &entryLastBlockResponse)

	if entryLastBlockResponse.Data.LastBlockScanned == entryLastBlock.Data.LastBlockScanned {
		log.Debug().Msgf("LastBlockScanned successfully changed to ", entryLastBlockResponse.Data.LastBlockScanned)
	}

	log.Debug().Msg("UpdateEntryLastBlock status = " + resp.Status)

}
