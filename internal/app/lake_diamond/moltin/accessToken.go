package moltin

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func updateAccessToken(app *application.Application) {
	log.Info().Msg("Updating access token")
	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/oauth/access_tokens"
	log.Debug().Msgf("Sending POST request to: %s", moltinUrl)

	// Add form data
	v := url.Values{}
	v.Set("grant_type", "client_credentials")
	v.Set("client_secret", app.Config.MoltinConfig.ClientSecret)
	v.Set("client_id", app.Config.MoltinConfig.ClientId)

	s := v.Encode()

	req, err := http.NewRequest("POST", moltinUrl, strings.NewReader(s))
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Response status: %s", resp.Status)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("ioutil.ReadAll() error")
		return
	}

	err = json.Unmarshal(data, &app.Moltin.MoltinToken)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
	}
}

func checkTokenValidity(app *application.Application) {
	timestamp := time.Now().UTC().Unix()

	if timestamp >= app.Moltin.MoltinToken.Expires {
		updateAccessToken(app)
	}
}
