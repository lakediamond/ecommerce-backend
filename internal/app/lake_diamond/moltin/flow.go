package moltin

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
)

type Flow struct {
	Data struct {
		Type        string `json:"type"`
		Name        string `json:"name"`
		Slug        string `json:"slug"`
		Description string `json:"description"`
		Enabled     bool   `json:"enabled"`
	} `json:"data"`
}

type FlowEntry struct {
	Data struct {
		Type                 string `json:"type"`
		TokenContractAddress string `json:"token_contract_address"`
		TokenSymbol          string `json:"token_symbol"`
		TokenInitialSupply   string `json:"token_initial_supply"`
		TokenTotalSupply     string `json:"token_total_supply"`
		LastBlockScanned     string `json:"last_block_scanned"`
	} `json:"data"`
}

type FlowEntryResponse struct {
	Data struct {
		ID                   string `json:"id"`
		Type                 string `json:"type"`
		TokenContractAddress string `json:"token_contract_address"`
		TokenInitialSupply   int    `json:"token_initial_supply"`
		TokenTotalSupply     int    `json:"token_total_supply"`
		LastBlockScanned     int    `json:"last_block_scanned"`
		Meta                 struct {
			Timestamps struct {
				CreatedAt time.Time `json:"created_at"`
				UpdatedAt time.Time `json:"updated_at"`
			} `json:"timestamps"`
		} `json:"meta"`
		Links struct {
			Self string `json:"self"`
		} `json:"links"`
	} `json:"data"`
}

//func to create flow to moltin platform. Using for store initial contract data.
func createFlow(app *application.Application) {
	log.Info().Msg("creating Flow")
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/flows"
	log.Debug().Msgf("Sending request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	var flow Flow
	flow.Data.Type = "flow"
	flow.Data.Name = "TokenContractConfiguration"
	flow.Data.Slug = "TokenContractConfiguration"
	flow.Data.Description = "Object to store token contract meta info"
	flow.Data.Enabled = true

	jsonStr, err := json.Marshal(flow)

	req, err := http.NewRequest("POST", moltinUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Msgf("http.NewRequest() error: %v\n", err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Msgf("http.Do() error: %v\n", err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)

	log.Debug().Msgf("Response status: %s", resp.Status)
	if resp.Status == "422 Unprocessable entity" {
		return
	}

	var flowEntryResponse FlowEntryResponse

	json.Unmarshal(data, &flowEntryResponse)

	setFlowFields(flowEntryResponse.Data.ID, app)
}

type FlowField struct {
	Data struct {
		Type          string `json:"type"`
		FieldType     string `json:"field_type"`
		Name          string `json:"name"`
		Slug          string `json:"slug"`
		Description   string `json:"description"`
		Unique        bool   `json:"unique"`
		Enabled       bool   `json:"enabled"`
		Required      bool   `json:"required"`
		Relationships struct {
			Flow struct {
				Data struct {
					Type string `json:"type"`
					ID   string `json:"id"`
				} `json:"data"`
			} `json:"flow"`
		} `json:"relationships"`
	} `json:"data"`
}

//forming flow fields and send to moltin
func setFlowFields(id string, app *application.Application) {
	var flowField FlowField

	flowField.Data.Type = "field"
	flowField.Data.FieldType = "string"
	flowField.Data.Name = "token_contract_address"
	flowField.Data.Slug = "token_contract_address"
	flowField.Data.Description = "token_contract_address"
	flowField.Data.Unique = false
	flowField.Data.Enabled = true
	flowField.Data.Required = false
	flowField.Data.Relationships.Flow.Data.Type = "flow"
	flowField.Data.Relationships.Flow.Data.ID = id

	log.Info().Msg("Setting field token_contract_address")
	sendSetFlow(flowField, app)

	log.Info().Msg("Setting field token_symbol")
	flowField.Data.Name = "token_symbol"
	flowField.Data.Slug = "token_symbol"
	flowField.Data.Description = "token_symbol"
	sendSetFlow(flowField, app)

	log.Info().Msg("Setting field token_initial_supply")
	flowField.Data.Name = "token_initial_supply"
	flowField.Data.Slug = "token_initial_supply"
	flowField.Data.Description = "token_initial_supply"
	sendSetFlow(flowField, app)

	log.Info().Msg("Setting field token_total_supply")
	flowField.Data.Name = "token_total_supply"
	flowField.Data.Slug = "token_total_supply"
	flowField.Data.Description = "token_total_supply"
	sendSetFlow(flowField, app)

	log.Info().Msg("Setting field last_block_scanned")
	flowField.Data.Name = "last_block_scanned"
	flowField.Data.Slug = "last_block_scanned"
	flowField.Data.Description = "last_block_scanned"
	sendSetFlow(flowField, app)

}

func sendSetFlow(flowField FlowField, app *application.Application) {
	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/fields"
	log.Debug().Msgf("Sending request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	jsonStr, err := json.Marshal(flowField)

	req, err := http.NewRequest("POST", moltinUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Msgf("http.NewRequest() error: %v\n", err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Msgf("http.Do() error: %v\n", err)
	}
	defer resp.Body.Close()
	log.Debug().Msgf("Response status: %s", resp.Status)
}

//initial flows are empty so we're need to fill them
func createFlowEntry(address, tokenSymbol, initialSupply, totalSupply, lastBlock string, app *application.Application) {
	log.Info().Msg("creating Flow entry")
	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/flows/" + app.Moltin.TokenContractConfigEntrySlug + "/entries"
	log.Debug().Msgf("Sending request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	var flow FlowEntry

	flow.Data.Type = "entry"
	flow.Data.TokenContractAddress = address
	flow.Data.TokenInitialSupply = initialSupply
	flow.Data.TokenSymbol = tokenSymbol
	flow.Data.TokenTotalSupply = totalSupply
	flow.Data.LastBlockScanned = lastBlock

	jsonStr, err := json.Marshal(flow)

	req, err := http.NewRequest("POST", moltinUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Msgf("http.NewRequest() error: %v\n", err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Msgf("http.Do() error: %v\n", err)
	}
	defer resp.Body.Close()

	log.Debug().Msgf("Response status: %s", resp.Status)

	var flowEntryResponse FlowEntryResponse

	data, err := ioutil.ReadAll(resp.Body)

	json.Unmarshal(data, &flowEntryResponse)

	app.Moltin.EntriesData.Entities[0].EntriesId = flowEntryResponse.Data.ID
}
