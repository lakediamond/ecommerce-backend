package moltin

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/rs/zerolog/log"
	"gitlab.com/lakediamond/ecommerce-backend/internal/pkg/application"
	appContext "gitlab.com/lakediamond/ecommerce-backend/internal/pkg/context"
	"gitlab.com/lakediamond/ecommerce-backend/pkg/handlers"
)

type EventReceived struct {
	Id          string       `json:"id"`
	Integration *Integration `json:"integration"`
}

type Integration struct {
	Name string `json:"name"`
}

type CurrencyEvent struct {
	Id        string `json:"id"`
	Resources string `json:"resources"`
}

type EventResources struct {
	Data struct {
		ID                string  `json:"id"`
		Type              string  `json:"type"`
		Code              string  `json:"code"`
		ExchangeRate      float64 `json:"exchange_rate"`
		Format            string  `json:"format"`
		DecimalPoint      string  `json:"decimal_point"`
		ThousandSeparator string  `json:"thousand_separator"`
		DecimalPlaces     int     `json:"decimal_places"`
		Default           bool    `json:"default"`
		Enabled           bool    `json:"enabled"`
		Links             struct {
			Self string `json:"self"`
		} `json:"links"`
		Meta struct {
			Timestamps struct {
				CreatedAt time.Time `json:"created_at"`
				UpdatedAt time.Time `json:"updated_at"`
			} `json:"timestamps"`
		} `json:"meta"`
	} `json:"data"`
}

var ContractData EventResources

var GetMoltinEvents = handlers.ApplicationHandler(newMoltinEventReceived)

//handling new request from moltin server, it must be currency event
func newMoltinEventReceived(w http.ResponseWriter, r *http.Request) {
	log.Info().Msg("New moltin event received, parsing data")
	app := appContext.GetApplicationContext(r)
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Error().Err(err).Msg("Cannot get moltin event data")
	}

	var events EventReceived

	err = json.Unmarshal(body, &events)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
	}

	var result = http.StatusOK

	log.Debug().Msgf("data parsed successfully: %s", events)

	if events.Integration.Name == app.Moltin.Events.CurrencyEventHandlerName {
		result = currencyEventHandler(body, app)
	}

	w.WriteHeader(result)
	return
}

//handling currency event
func currencyEventHandler(body []byte, app *application.Application) int {
	var event CurrencyEvent

	err := json.Unmarshal(body, &event)
	if err != nil {
		log.Error().Err(err).Msg("Error response parsing")
	}

	err = json.Unmarshal([]byte(event.Resources), &ContractData)
	if err != nil {
		log.Error().Err(err).Msg("unmarshal event error")
	}

	log.Debug().Msgf("contract data: %s", ContractData)

	if isEther(ContractData.Data.Format) {
		err := initNewContract(&ContractData, app)
		if err != nil {
			return http.StatusBadRequest
		}
		return http.StatusCreated
	}
	return http.StatusOK
}

//forming data to moltin to store contract info
func initNewContract(data *EventResources, app *application.Application) error {
	log.Info().Msg("initializing new contract")
	getInitialContractInfo(data, app)

	checkTokenValidity(app)

	moltinUrl := app.Config.MoltinConfig.BaseUrl + "/v2/currencies/" + data.Data.ID
	log.Debug().Msgf("Sending PUT request to: %s", moltinUrl)

	bearer := "Bearer " + app.Moltin.MoltinToken.AccessToken

	jsonStr, err := json.Marshal(data)

	req, err := http.NewRequest("PUT", moltinUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		log.Error().Err(err).Msg("http.NewRequest() error")
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("http.Do() error")
		return err
	}
	defer resp.Body.Close()

	log.Info().Msgf("response status: %s", resp.Status)

	return nil
}

//getting info from smart contract
func getInitialContractInfo(data *EventResources, app *application.Application) {
	log.Info().Msg("Getting initial contract information")
	app.EthConfig.EthFilter.Addresses = []common.Address{common.HexToAddress(data.Data.Format)}

	address := common.HexToAddress(data.Data.Format)
	log.Info().Msgf("%v", data.Data)
	log.Info().Msgf("getInitialContractInfo contract address: %s", address.String())


	instance, err := NewToken(address, app.EthConfig.EthClient)
	if err != nil {
		log.Error().Err(err).Msg("Cannot initialize new token")
	}

	opts := bind.CallOpts{Pending: true, From: common.HexToAddress(data.Data.Format), Context: context.Background()}

	data.Data.Code, err = instance.Symbol(&opts)
	if err != nil {
		log.Error().Err(err).Msg("Cannot initialize token symbol")
	}

	precision, err := instance.Decimals(&opts)
	if err != nil {
		log.Error().Err(err).Msg("Cannot initialize token decimals")
	}

	data.Data.DecimalPlaces, err = strconv.Atoi(precision.String())

	if err != nil {
		log.Error().Err(err).Msg("Decimal converting error")
	}

	var entries application.Entities
	entries.TokenContractAddress = data.Data.Format
	entries.TokenSymbol = data.Data.Code
	data.Data.Format = "{price} " + data.Data.Code

	if len(app.Moltin.EntriesData.Entities) == 0 {
		app.Moltin.EntriesData.Entities = append(app.Moltin.EntriesData.Entities, entries)
	} else {
		app.Moltin.EntriesData.Entities[0] = entries
	}

	data.Data.Meta.Timestamps.UpdatedAt = time.Now()

	createFlow(app)
	createFlowEntry(app.EthConfig.EthFilter.Addresses[0].Hex(), data.Data.Code, "0", "0", "0", app)
}

//to check is it valid ethereum address
func isEther(s string) bool {
	s = strings.TrimPrefix(s, "0x")
	_, err := hex.DecodeString(s)
	if err == nil {
		return true
	}
	return false
}
